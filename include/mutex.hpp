#ifndef MUTEXEXAMPLE_HPP
#define MUTEXEXAMPLE_HPP

#include <systemc.h>

SC_MODULE(MutexExample) 
{
  sc_core::sc_mutex family_car;
  sc_core::sc_time hrs;
  
  SC_CTOR(MutexExample)
  : hrs(1,sc_core::SC_SEC) /* 1 ns = 1 hr */
  {
    SC_THREAD(parent_thread  );
    SC_THREAD(teen_thread    );
  }

  // Processes
  void parent_thread(void);
  void teen_thread(void);

  // Helper methods
  void delay(sc_core::sc_time t, sc_core::sc_time& miss, int uncertainty); // Wait +/- mins
  void note(const char* who, const char* message); // Display time & message
};

#endif
