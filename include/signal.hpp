#ifndef SIGNAL_HPP
#define SIGNAL_HPP
#include <systemc.h>
#include <string>

SC_MODULE(SignalExample) {
  enum color {BLACK, RED, GREEN, BLUE, YELLOW, MAGENTA, CYAN, WHITE};
  // Local data variables
  int                             count;       
  color                           traffic_temp;
  std::string                     message_temp;
  // Local channels
  sc_core::sc_signal<int>         count_sig;  
  sc_core::sc_signal<color>       traffic_sig;
  sc_core::sc_signal<std::string> message_sig;
  // Constructor
  SC_CTOR(SignalExample)
  {
    SC_THREAD(signal_ex_thread);
    SC_METHOD(signal_ex_method);
      sensitive << count_sig
                << traffic_sig
                << message_sig;
  }
  void signal_ex_thread(void);
  void signal_ex_method(void);
};

#endif
