#ifndef FIFO_HPP
#define FIFO_HPP

#include <systemc.h>

SC_MODULE(FifoFir) 
{
  sc_core::sc_fifo<double> orig_in;
  sc_core::sc_fifo<double> data_in;
  sc_core::sc_fifo<double> data_out;
  double*     m_pipe;     // Data pipe
  double*     m_coeff;    // Array of coefficients
  unsigned m_taps;     // Number of coefficients
  unsigned m_tap;      // Current tap
  const char* m_cfg_filename;
  
  SC_HAS_PROCESS(FifoFir);
  // Constructor
  FifoFir(sc_core::sc_module_name _name
          , const char* _cfg_filename="../test/fifo_fir.cfg"
          );
  
  // Destructor
  ~FifoFir() 
  {
    if (m_taps) 
    {
      delete[] m_coeff;
      delete[] m_pipe;
    }
  }

  // Processes
  void stimulus_thread(void);
  void sc_fifo_ex_thread(void);
  void results_thread(void);
};

#endif
