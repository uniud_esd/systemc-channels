#include <iostream>
#include <string>
#include <systemc.h>
#include "signal.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) {
  cout << "INFO: Elaborating" << endl;
  SignalExample signal_ex_i("signal_ex_i");
  cout << "INFO: Simulating" << endl;
  sc_start();

  cout << "INFO: Post-processing" << endl;

  return 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//END $Id$
