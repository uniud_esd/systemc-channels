#include <iostream>
#include <systemc.h>
#include "mutex.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) 
{
  MutexExample mutex_ex_i("mutex_ex_i");
  
  cout << "INFO: Starting simulation" << endl;
  sc_time max_sim(96,SC_SEC);
  sc_start(max_sim);

  cout << "INFO: Simulation ended" << endl;

  return 0;
}
