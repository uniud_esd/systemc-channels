#include <iostream>
#include <systemc.h>
#include "semaphore.hpp"

using namespace std;

int sc_main(int argc, char* argv[]) 
{
  SemaphoreExample semaphore_ex_i("semaphore_ex_i");
  
  cout << "INFO: Starting simulation" << endl;
  sc_start();
  cout << "INFO: Exiting simulation" << endl;

  return 0;
}
