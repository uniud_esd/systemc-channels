#include <iostream>
#include <systemc.h>
#include "fifo.hpp"

using namespace std;
unsigned errors = 0;

int sc_main(int argc, char* argv[]) 
{
  FifoFir sc_fifo_ex_i("sc_fifo_ex_i");
  cout << "INFO: Starting simulation" << endl;
  
  sc_start();

  cout << "INFO: Exiting  simulation" << endl;
  cout << "INFO: Simulation " << (errors?"FAILED":"PASSED")
       << " with " << errors << " errors"
       << endl;
  return errors?1:0;
}
