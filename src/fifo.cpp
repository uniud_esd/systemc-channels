#include <fstream>
#include <iomanip>
#include <cmath>
#include "fifo.hpp"

extern unsigned errors;

using namespace std;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Constructor
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
FifoFir::FifoFir(sc_module_name _name
                  , const char* _cfg_filename) 
  : sc_module(_name)
  , m_cfg_filename(_cfg_filename)
  , m_taps(0)
  , m_tap(0)
  , orig_in(32)
  , data_in(32)
  , data_out(32)
{
  SC_THREAD(stimulus_thread);
  SC_THREAD(sc_fifo_ex_thread);
  SC_THREAD(results_thread);
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // Read coefficients from configuration file and initialize pipe to zero
  //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  ifstream cfg_file(m_cfg_filename);
  if (!cfg_file) 
  {
    cerr << "ERROR: Problem opening " << m_cfg_filename << " for input." << endl;
    errors++;
    return;
  }
  
  cfg_file >> m_taps;
  if (m_taps < 1) {
    cerr << "ERROR: Number of taps must be greater than zero!" << endl;
    errors++;
    return;
  }
  
  // Allocate memory
  m_pipe  = new double[m_taps];
  m_coeff = new double[m_taps];
  for (unsigned tap=0;tap!=m_taps;tap++) 
  {
    if (cfg_file.eof()) 
    {
      cerr << "ERROR: Too few coefficients when reading [" << tap << "]." << endl;
      errors++;
      return;
    }
    cfg_file >> m_coeff[tap];
    m_pipe[tap] = 0;
  }
  
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Processes
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void FifoFir::stimulus_thread(void) 
{
  unsigned PTS=20;
  for (unsigned t=0;t<PTS;t++) 
  {
    double data = 0.0;
    if (t==10) data = 1.0; // Impulse
    orig_in.write(data);
    data_in.write(data);
  }
  wait(SC_ZERO_TIME);
  
}

void FifoFir::sc_fifo_ex_thread(void) 
{
  for(;;) 
  {
    unsigned coeff = m_tap; // Used to index coeffiecients
    // Read next piece of data
    double data = data_in.read();
    m_pipe[m_tap++] = data;
    if (m_tap == m_taps) m_tap = 0; // Wrap
    double result = 0;
    for (unsigned tap=0;tap!=m_taps;tap++,coeff++) 
    {
      if (coeff == m_taps) coeff = 0; // Wrap
      result += m_coeff[coeff] * m_pipe[tap];
    }
    data_out.write(result);
  }
  
}

void FifoFir::results_thread(void) 
{
  for(unsigned i=0;;i++) 
  {
    double data =  orig_in.read();
    double result =  data_out.read();
    cout << "DATA: "
         << "["  << setw(2) << i << "]" 
         << "= " << setw(9) << fixed << setprecision(5) << data
         << " "  << setw(9) << fixed << setprecision(5) << result
         << endl;
  }
}
