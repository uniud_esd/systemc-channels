#include "mutex.hpp"
#include <iostream>
#include <iomanip>

using namespace std;

void MutexExample::parent_thread  (void) 
{
  const char * me("Parent");
  sc_time fudge(SC_ZERO_TIME);
  wait(6*hrs); // Start day at 6 am
  
  for(;;) 
  {
    note(me,"Wake up");
    delay(2*hrs,fudge,30);
    if (family_car.trylock() != -1) {
      note(me,"Drive to work");
      delay(1*hrs,fudge,30);
      family_car.unlock();
      note(me,"Begin to work");
      delay(3*hrs,fudge,5);
      if (family_car.trylock() != -1) {
        note(me,"Go to lunch");
        delay(1*hrs,fudge,15);
        family_car.unlock();
      } else {
        note(me,"Eat in - gripe at kid");
        delay(1*hrs,fudge,15);
      }
      note(me,"Return to work");
      delay(3*hrs,fudge,15);
      note(me,"Attempting to get car back");
      family_car.lock();
      note(me,"Drive home");
      delay(1*hrs,fudge,30);
      family_car.unlock();
    } else {
      note(me,"Take bus to work");
      delay(9*hrs,fudge,45);
      note(me,"Take bus home");
    }
    note(me,"Do chores and eat supper");
    delay(2*hrs,fudge,30);
    if (family_car.trylock() != -1) {
      note(me,"Drive to meeting");
      delay(3*hrs,fudge,30);
      note(me,"Drive to home");
      family_car.unlock();
    } else {
      note(me,"Take a walk");
      delay(3*hrs,fudge,10);
    }
    note(me,"Watch TV");
    delay(1*hrs,fudge,5);
    note(me,"Go to bed");
    delay(7*hrs,fudge,30);
  }
  
}

void MutexExample::teen_thread(void) 
{
  const char * me("Teen");
  sc_time fudge(SC_ZERO_TIME);
  wait(10*hrs); // Start day at 10 am
  
  for(;;) 
  {
    note(me,"Wake up");
    delay(2*hrs,fudge,30);
    if (family_car.trylock() != -1) {
      switch(rand()%4) {
        case 0: note(me,"Drive to school"); break;
        case 1: note(me,"Drive to mall"); break;
        case 2: note(me,"Drive to friends house"); break;
        case 3: note(me,"Drive to work"); break;
      }
      delay(2*hrs,fudge,60);
      note(me,"Return car");
      family_car.unlock();
    } else {
      switch(rand()%4) {
        case 0: note(me,"Do schoolwork"); break;
        case 1: note(me,"Earn money"); break;
        case 2: note(me,"Do chores"); break;
        case 3: note(me,"Party"); break;
      }
      delay(2*hrs,fudge,60);
    }
    note(me,"Sleep");
    delay(2*hrs,fudge,60);
  } 
}

void MutexExample::delay(sc_time t, sc_time& fudge, int uncertainty) 
{
    int r = rand() % uncertainty;
    // Add previous fudge back in to make up for previous uncertainty
    t += fudge;
    // Compute fudge for this time
    fudge = (r - uncertainty/2)/60.0*hrs;
    // Calculate actual delay
    sc_time d = t - fudge;
    if (d < SC_ZERO_TIME) d = SC_ZERO_TIME; // Cannot have a negative delay
    wait(d);
}

void MutexExample::note(const char* who, const char* message) 
{
  double t = sc_time_stamp().to_seconds();
  long   s = int(t);
  unsigned  h = s % 12;
  unsigned  m = int((t - double(s))* 60.0);
  bool     am = (s % 24)<12;
  cout << "NOTE: " 
       << setw(2) << h << ":"
       << setw(2) << setfill('0') << m << " "
       << (am?"AM":"PM")  << " "
       << who << ": "
       << message 
       << endl;
}
