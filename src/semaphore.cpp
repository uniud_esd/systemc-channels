#include <iostream>
#include <iomanip>
#include "semaphore.hpp"

using namespace std;

void SemaphoreExample::customer1_thread(void) 
{
  customer("customer1");
}

void SemaphoreExample::customer2_thread(void) 
{
  customer("customer2");
}

void SemaphoreExample::customer3_thread(void) 
{
  customer("customer3");
}

void SemaphoreExample::customer4_thread(void) 
{
  customer("customer4");
}

void SemaphoreExample::customer5_thread(void) 
{
  customer("customer5");
}

// Common behavior
void SemaphoreExample::customer(const char* who) 
{
  for(;;) 
  {
    delay(60*MINS,30);
    note(who,"Empty");
    if (!fills) break;
    pump.wait();
    e_change.notify();
    note(who,"Filling up");
    delay(15*MINS,120);
    fills--;
    pump.post();
    e_change.notify();
    note(who,"Full");
  }
}

void SemaphoreExample::monitor_method(void) 
{
  cout << "INFO: Monitor " << pump.get_value() << " available" << endl;
}

void SemaphoreExample::delay(sc_time t,int uncertainty) 
{
    int r = rand() % uncertainty;
    // Calculate actual delay
    sc_time d = t - (r - uncertainty/2)/60.0*MINS;
    if (d < SC_ZERO_TIME) d = SC_ZERO_TIME; // Cannot have a negative delay
    wait(d);
  
}

void SemaphoreExample::note(const char* who, const char* message) 
{
  double t = sc_time_stamp().to_seconds();
  unsigned  m = int(t);
  unsigned  h = m/60;
  m -= 60*h;
  cout << "NOTE: " 
       << setw(2) << h << ":"
       << setw(2) << setfill('0') << m << " "
       << who << ": "
       << message 
       << endl;
}
